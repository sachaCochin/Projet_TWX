var default_app_port = 3000;

var express = require('express'),
	exphbs = require('express-handlebars'),
	body_parser = require('body-parser'),
	routes = require('./routes/index'),
	mkdirp = require('mkdirp'),
	session = require('express-session');

mkdirp(__dirname + "/temp", function(err) {});

var app=express();
app.engine('.hbs', exphbs({defaultLayout: 'default_layout', extname: '.hbs'}));
app.set('view engine', '.hbs');

app.use(body_parser.json());
app.use(body_parser.urlencoded({extended: true}));
app.use(require('express-session')({
    secret: 'keyboard cat',
  	truecookie: { maxAge: 60000 }, 
  	resave: false,
  	saveUninitialized: false
}));

app.use(express.static(__dirname + '/public'));

app.use('/', routes);

app.listen(default_app_port);
console.log("Server started at : http://localhost:" + default_app_port + "/");

module.exports = app;